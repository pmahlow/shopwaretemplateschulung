<?php

namespace Shopware\Themes\MittwaldSchulung;

use Shopware\Components\Form as Form;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';

    protected $name = <<<'SHOPWARE_EOD'
Mittwald Schulung
SHOPWARE_EOD;

    protected $description = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

    protected $author = <<<'SHOPWARE_EOD'
Philipp Mahlow
SHOPWARE_EOD;

    protected $license = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;


    /*
     * Hier werden unsere eigenen Scripts registriert.
     */

    protected $javascript = array(
        'src/js/jquery.add-article.js',
        'src/js/jquery.last-seen-products.js'
    );

    /**
     * Erstelle unsere individuelle Theme-Konfiguration
     *
     * @param Form\Container\TabContainer $container
     */
    public function createConfig(Form\Container\TabContainer $container)
    {
        // erstelle einen Tab "Spezial Einstellungen"
        $tab = $this->createTab('spezial-einstellungen', 'Spezial Einstellungen');

        // erstelle ein Fieldset "Spezial Farben"
        $fieldset = $this->createFieldSet('spezial-fieldset', 'Spezial Farben');

        // erstelle ein ColorPicker-Feld für den Body-Background
        $field = $this->createColorPickerField(
            'body-background',
            '@body-background',
            '#000000'
        );

        // für das Element dem Fieldset hinzu
        $fieldset->addElement($field);

        // erstelle ein weiteres Color-Picker-Field
        $field = $this->createColorPickerField(
            'footer-background',
            '@footer-background',
            '#000000'
        );

        // ... und füge auch dieses zum Fieldset hinzu
        $fieldset->addElement($field);

        // fieldset dem Tab hinzufügen
        $tab->addElement($fieldset);

        // tab dem Container hinzufügen
        $container->addTab($tab);
    }
}