<?php
/**
 *
 * Beispiel für einen eigenen smarty modifier
 *
 * Ersetzt alle Umlaute in einem String
 *
 * Beispiel: {$sArticle.articleName|umlauts}
 *
 * @param $string
 * @return mixed
 */
function smarty_modifier_umlauts($string)
{
    $search = array(
        'ä',
        'Ä',
        'ö',
        'Ö',
        'ü',
        'Ü',
        'ß'
    );

    $replace = array(
        'ae',
        'Ae',
        'oe',
        'Oe',
        'ue',
        'Ue',
        'ss'
    );

    return str_replace($search, $replace, $string);
}