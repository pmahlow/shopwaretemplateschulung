{extends file="parent:frontend/detail/index.tpl"}

{block name='frontend_detail_data_attributes' append}

   {* Beispielanpassungen der Detailseite *}

    {* Product attribute 4 *}
    {block name='frontend_detail_data_attributes_attr4'}
        {if $sArticle.attr4}
            <li class="base-info--entry entry-attribute">
                <strong class="entry--label">
                    {s name="DetailAttributeField4Label"}Meine Super Info{/s}:
                </strong>

                <span class="entry--content">
                    {$sArticle.attr4|escape}
                </span>
            </li>
        {/if}
    {/block}

    {* Product dimensions *}
    {block name='frontend_detail_data_attributes_dimensions'}

        {* nur anzeigen, wenn Länge und Breite gesetzt ist! *}
        {if $sArticle.length AND $sArticle.width}
            <li class="base-info--entry entry-attribute">
                <strong class="entry--label">
                    {s name="DetailAttributeFieldDimensionLabel"}Abmessungen{/s}:
                </strong>

                <span class="entry--content">
                    {$sArticle.width} x {$sArticle.length}
                    {if $sArticle.height} x {$sArticle.height} {/if}
                </span>
            </li>
        {/if}

    {/block}
{/block}