/**
 * override für das JavaScript Plugin swAddArticle
 */

;(function($, window) {
    'use strict';

    $.overridePlugin('swAddArticle', {
        /**
         *
         * nur die Funktion sendSerializedForm wird überschrieben.
         *
         * Hier wird eine confirm-Abfrage um den AJAX-Call gelegt.
         *
         * @param event
         */
        sendSerializedForm: function (event) {
            event.preventDefault();

            var me = this,
                opts = me.opts,
                $el = me.$el,
                ajaxData = $el.serialize();

            ajaxData += '&isXHR=1';

            if (opts.showModal) {
                $.overlay.open({
                    'closeOnClick': false
                });

                $.loadingIndicator.open({
                    'openOverlay': false
                });
            }

            // hier ist die Anpassung
            if(confirm('Bist du dir wirklich sicher?')) {
                $.publish('plugin/swAddArticle/onBeforeAddArticle', [me, ajaxData]);

                $.ajax({
                    'data': ajaxData,
                    'dataType': 'jsonp',
                    'url': opts.addArticleUrl,
                    'success': function (result) {
                        $.publish('plugin/swAddArticle/onAddArticle', [me, result]);

                        if (!opts.showModal) {
                            return;
                        }

                        $.loadingIndicator.close(function () {
                            $.modal.open(result, {
                                width: 750,
                                sizing: 'content',
                                onClose: me.onCloseModal.bind(me)
                            });

                            picturefill();

                            StateManager.updatePlugin(opts.productSliderSelector, 'swProductSlider');

                            $.publish('plugin/swAddArticle/onAddArticleOpenModal', [me, result]);
                        });
                    }
                });
            }
        }

    });
})(jQuery, window);
