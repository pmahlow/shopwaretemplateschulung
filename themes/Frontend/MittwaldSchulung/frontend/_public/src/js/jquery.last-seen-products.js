/**
 * override für das JavaScript Plugin swLastSeenProducts
 *
 * rendered den Preis mit in den Produkttitel im Slider der zuletzt
 * angesehen Artikel
 *
 * @see frontend/index/index.tpl
 * Hier wird der Preis in die Config übernommen!
 */


;(function ($) {

    var emptyObj = {};

    $.overridePlugin('swLastSeenProducts', {
        createProductTitle: function (data) {
            var me = this,
                $title = $('<a>', {
                    'rel': 'nofollow',
                    'class': me.opts.titleCls,
                    'title': data.articleName,
                    'href': data.linkDetailsRewritten,
                    // hier kommt die Anpassung
                    'html': data.articleName + " (" + data.price + ")"
                });

            $.publish('plugin/swLastSeenProducts/onCreateProductTitle', [ me, $title, data ]);

            return $title;
        }
    });
}(jQuery));
