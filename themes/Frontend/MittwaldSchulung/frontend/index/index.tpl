{extends file="parent:frontend/index/index.tpl"}

{*
   Überschreiben des Blocks, um den Preis für den
   Slider mit den zuletzt angesehen Artikeln zur
   Verfügung zu stellen.
 *}
{block name="frontend_index_header_javascript"}
    <script type="text/javascript">
        //<![CDATA[
        {block name="frontend_index_header_javascript_inline"}
        var timeNow = {time() nocache};

        var controller = controller || {ldelim}
                    'vat_check_enabled': '{config name='vatcheckendabled'}',
                    'vat_check_required': '{config name='vatcheckrequired'}',
                    'ajax_cart': '{url controller='checkout' action='ajaxCart'}',
                    'ajax_search': '{url controller="ajax_search"}',
                    'ajax_login': '{url controller="account" action="ajax_login"}',
                    'register': '{url controller="register"}',
                    'checkout': '{url controller="checkout"}',
                    'ajax_logout': '{url controller="account" action="ajax_logout"}',
                    'ajax_validate': '{url controller="register"}',
                    'ajax_add_article': '{url controller="checkout" action="addArticle"}',
                    'ajax_listing': '{url module="widgets" controller="Listing" action="ajaxListing"}',
                    'ajax_cart_refresh': '{url controller="checkout" action="ajaxAmount"}'
                    {rdelim};

        var snippets = snippets || {ldelim}
                    'noCookiesNotice': '{s name="IndexNoCookiesNotice"}{/s}'
                    {rdelim};

        var themeConfig = themeConfig || {ldelim}
                    'offcanvasOverlayPage': '{$theme.offcanvasOverlayPage}'
                    {rdelim};

        var lastSeenProductsConfig = lastSeenProductsConfig || {ldelim}
                    'baseUrl': '{$Shop->getBaseUrl()}',
                    'shopId': '{$Shop->getId()}',
                    'noPicture': '{link file="frontend/_public/src/img/no-picture.jpg"}',
                    'productLimit': ~~('{config name="lastarticlestoshow"}'),
                    'currentArticle': {ldelim}{if $sArticle}
                        {foreach $sLastArticlesConfig as $key => $value}
                        '{$key}': '{$value}',
                        {/foreach}
                        'articleId': ~~('{$sArticle.articleID}'),
                        {* hier ist die Anpassung!*}
                        'price': '{$sArticle.price|currency}',
                        'linkDetailsRewritten': '{$sArticle.linkDetailsRewrited}',
                        'articleName': '{$sArticle.articleName|escape:"javascript"}',
                        'imageTitle': '{$sArticle.image.description|escape:"javascript"}',
                        'images': {ldelim}
                            {foreach $sArticle.image.thumbnails as $key => $image}
                            '{$key}': {ldelim}
                                'source': '{$image.source}',
                                'retinaSource': '{$image.retinaSource}',
                                'sourceSet': '{$image.sourceSet}'
                                {rdelim},
                            {/foreach}
                            {rdelim}
                        {/if}{rdelim}
                    {rdelim};
        {/block}
        //]]>
    </script>

    {if $theme.additionalJsLibraries}
        {$theme.additionalJsLibraries}
    {/if}
{/block}