{extends file="parent:frontend/index/footer-navigation.tpl"}

{block name="frontend_index_footer_column_newsletter"}

    vor

    {* Beispiele für Textbausteine: *}
    {s name="IndexSearchFieldPlaceholder"}Test123{/s}
    {s name="IndexSearchFieldPlaceholder" namespace="SomeOtherNamespace"}Test123{/s}

    {$smarty.block.parent}
    nach
{/block}


{* Austauschen des Hotline-Blocks - hier ist die Liste "Unsere Vorteile" *}
{block name="frontend_index_footer_column_service_hotline"}
    <div class="footer--column column--hotline is--first block">
        <div class="column--headline">{s name="benefitHeader"}Unsere Vorteile{/s}</div>

        {block name="frontend_index_footer_column_service_hotline_content"}
            <div class="column--content">
                <ul>
                    <li>{s name="benefit1"}Test1{/s}</li>
                    <li>{s name="benefit2"}Test2{/s}</li>
                    <li>{s name="benefit3"}Test3{/s}</li>
                </ul>
            </div>
        {/block}
    </div>

    {* Beispiel für den Einsatz eines Widgets *}
    {action module="widgets" controller="checkout" action="info"}
{/block}