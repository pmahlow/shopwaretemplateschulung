{extends file="parent:frontend/listing/listing.tpl"}

{* rudimentäres Beispiel für ein minimales Listing *}
{block name="frontend_listing_listing_content"}
    <ul>
        {* Schleife für sArticles *}
        {foreach $sArticles as $sArticle}
            <li>
                <a href="{$sArticle.linkDetails}">
                    {$sArticle.articleName|umlauts|upper} ({$sArticle.price|currency})

                    <img alt="{$sArticle.articleName}"
                         srcset="{$sArticle.image.thumbnails.0.sourceSet}" />
                </a>
            </li>
        {/foreach}
    </ul>
    {debug}
{/block}