{extends file="parent:widgets/checkout/info.tpl"}

{block name="frontend_index_checkout_actions_notepad" prepend}
	<li class="navigation--entry entry--notepad" role="menuitem">
		<a href="https://facebook.com" title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkFacebook'}Facebook{/s}"|escape}" class="btn">
			<i class="icon--facebook2"></i>
		</a>
	</li>
{/block}
