<?php

/**
 * Class Shopware_Controllers_Frontend_PmTest
 *
 * Controller PmTest
 */
class Shopware_Controllers_Frontend_PmTest
    extends Enlight_Controller_Action
{
    /**
     * standard action
     */
    public function indexAction()
    {
        $this->View()->assign('Test', 'Test123');

        //get repository for article detail
        $repository = Shopware()
                        ->Models()
                        ->getRepository('Shopware\Models\Article\Detail');

        /**
         * @var \Shopware\Models\Article\Detail $articleDetail
         */
        $articleDetail = $repository->findOneBy(['number' => 'SW10153']);

        // artikelname an view übergeben
        $this->View()->assign('articleName', $articleDetail->getArticle()->getName());
    }

    /**
     * sag hallo
     */
    public function helloAction()
    {
        // parameter name auslesen
        $name = $this->Request()->getParam('name');

        // wenn name nicht gesetzt -> weiterleitung an index action
        if($name == NULL){
            $this->redirect(array(
                'controller' => 'PmTest',
                'action' => 'index'
            ));
            return;
        }

        // name an view übergeben
        $this->View()->assign('name', $name);
    }
}