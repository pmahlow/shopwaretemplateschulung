<?php

class Shopware_Plugins_Frontend_PmExample3_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{


    /**
     * Plugin installer
     *
     * @return bool
     */
    public function install(){

        // controller PmTest registrieren
        $this->registerController('frontend', 'PmTest');

        return TRUE;
    }

    /**
     * Returns plugin info
     *
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => '1.0.0',
            'label' => 'Beispiel Plugin 3',
            'author' => 'Philipp Mahlow',
            'copyright' => '2016 - Philipp Mahlow',
            'description' => 'Ein langer Text...'
        );
    }

}