{extends file='frontend/index/index.tpl'}

{* My Content *}
{block name="frontend_index_content"}
    {$articleName}

    <br/><br/>

    {* formular - als action wird url zu unserer helloAction generiert *}
    <form action="{url controller=PmTest action=hello}" method="POST">
        <label for="name">Name: </label>
        <input name="name" id="name" type="text"/><br/>
        <button type="submit">Sag Hallo!</button>
    </form>
{/block}