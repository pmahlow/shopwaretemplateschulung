<?php

class Shopware_Plugins_Frontend_PmExample4_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{


    /**
     * Plugin installer
     *
     * @return bool
     */
    public function install(){
        try {
            $this->subscribeEvent(
                'Enlight_Controller_Action_PostDispatch_Frontend_Detail',
                'onPostDispatch'
            );

            $this->registerController('frontend', 'PmExample4');


            // neues attribut für artikel registrieren
            Shopware()->Models()->addAttribute(
                's_articles_attributes',
                'Pm',
                'superField',
                'varchar(255)',
                true,
                null
            );

            // dynamisches attribute model für artikel generieren
            Shopware()->Models()->generateAttributeModels(array(
                's_articles_attributes'
            ));

            return TRUE;
        } catch (Exception $ex)
        {
            return array(
                'success' => FALSE,
                'message' => $ex->getMessage()
            );
        }
    }

    public function uninstall(){

        // hier kann das attribute wieder gelöscht werden...

        /*Shopware()->Models()->removeAttribute(
            's_articles_attributes',
            'Pm',
            'superField'
        );

        Shopware()->Models()->generateAttributeModels(array(
            's_articles_attributes'
        ));*/

        return true;
    }


    /**
     * @param Enlight_Event_EventArgs $args
     */
    public function onPostDispatch(Enlight_Event_EventArgs $args)
    {
        /**
         * @var Enlight_Controller_Action $subject
         */
        $subject = $args->getSubject();

        // eigenes template verzeichnis registrieren.

        // das ist nur für view anpassungen ohne eigenen controller notwendig.

        if($subject->Request()->getActionName() == 'index')
        {
            //view verzeichnis hinzufügen
            $subject->View()->addTemplateDir($this->Path() . 'Views/');
        }

    }


    /**
     * Returns plugin info
     *
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => '1.0.0',
            'label' => 'Beispiel Plugin 4',
            'author' => 'Philipp Mahlow',
            'copyright' => '2016 - Philipp Mahlow',
            'description' => 'Ein langer Text...'
        );
    }

}