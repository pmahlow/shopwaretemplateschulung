<?php

class Shopware_Controllers_Frontend_PmExample4
    extends Enlight_Controller_Action
{
    /**
     * @var \Shopware\Models\Article\Article
     */
    protected $article;


    public function preDispatch()
    {
        /*
         * perDispatch ist das equivalent zum preDispatch-Event.
         *
         * wird vor der Ausführung jeder Action des Controllers ausgeführt und kann zum Beispiel
         * für Validierungszwecke benutzt werden, wenn alle Actions gleiche Parameter haben.
         */

        $articleId = $this->Request()->getParam('articleId');

        if($articleId) {

            $repository = Shopware()
                ->Models()
                ->getRepository('Shopware\Models\Article\Article');

            $this->article = $repository->find($articleId);

            if(!$this->article){
                $this->redirect(array(
                    'controller' => 'index'
                ));
            }
        }
    }


    public function indexAction(){

        // eigenes Attribut setzen
        $this->article->getAttribute()->setPmSuperField('TEST');

        // änderung persistieren
        Shopware()->Models()->persist($this->article->getAttribute());

        // alle änderungen in die DB schreiben
        Shopware()->Models()->flush();

        $this->redirect(array(
            'controller' => 'Detail',
            'sArticle' => $this->article->getId()
        ));
    }
}