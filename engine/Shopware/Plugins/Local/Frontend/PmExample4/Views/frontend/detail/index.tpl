{extends file="parent:frontend/detail/index.tpl"}

{block name='frontend_detail_data_attributes' append}
    {*
    View Anpassung für die artikeldetail-seite.
    Link zu unserem controller anzeigen. ArtikelID als Parameter übergeben.
    *}

    <a href="{url controller=PmExample4 action=index articleId=$sArticle.articleID}">Attribut schreiben</a>
{/block}