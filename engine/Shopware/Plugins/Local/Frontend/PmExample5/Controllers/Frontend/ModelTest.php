<?php

class Shopware_Controllers_Frontend_ModelTest
    extends Enlight_Controller_Action
{

    protected function getRepository(){
        return Shopware()
            ->Models()
            ->getRepository('Shopware\CustomModels\PmDoctrineModel\MeinModel');
    }

    public function indexAction(){
        /**
         * MeinModel[] $data
         */
        $data = $this->getRepository()->findAll();

        foreach($data as $item){
            echo $item->getName();
            echo '<br/>';
        }

        die;

    }

    public function createAction(){
        $name = $this->Request()->getParam('name', 'Hans-Peter');

        /*
         * neues model erstellen
         */
        $model = new \Shopware\CustomModels\PmDoctrineModel\MeinModel();
        $model->setName($name);


        // ... und speichern
        Shopware()->Models()->persist($model);
        Shopware()->Models()->flush();

        $this->forward('index');
    }

    public function deleteAction(){
        $name = $this->Request()->getParam('name');

        // model anhand von property name finden
        $model = $this->getRepository()->findOneBy(array(
            'name' => $name
        ));

        // datensatz löschen
        Shopware()->Models()->remove($model);
        Shopware()->Models()->flush();


        $this->forward('index');
    }


}