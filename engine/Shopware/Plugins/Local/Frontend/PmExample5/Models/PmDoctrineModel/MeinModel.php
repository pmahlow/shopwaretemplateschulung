<?php
/*
 * wichtig ist hier der Namespace! Erster Teil "Shopware\CustomModels" ist laut Konvention festgelegt.
 */
namespace Shopware\CustomModels\PmDoctrineModel;
use Shopware\Components\Model\ModelEntity,
    Doctrine\ORM\Mapping AS ORM;


/*
 * standard Doctrine Model.
 *
 * Siehe Doctrine Doku.
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="s_plugin_meine_tabelle")
 */
class MeinModel extends ModelEntity
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;


    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

}
