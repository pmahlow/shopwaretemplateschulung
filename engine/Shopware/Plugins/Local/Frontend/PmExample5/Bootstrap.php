<?php

class Shopware_Plugins_Frontend_PmExample5_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{


    public function afterInit()
    {
        // autoloading für eigene Models einrichten.
        $this->registerCustomModels();
    }


    public function install(){

        // eigenes model registrieren
        $this->updateSchema();

        $this->registerController('Frontend', 'ModelTest');

        return true;
    }



    protected function updateSchema()
    {
        $em = Shopware()->Models();
        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

        // meta daten des eigenen models sammeln
        $classes = array(
            $em->getClassMetadata('Shopware\CustomModels\PmDoctrineModel\MeinModel')
        );

        try {
            // tabelle löschen, falls schon vorhanden
            $tool->dropSchema($classes);
        } catch (Exception $e) {
            //ignore
        }

        // tabelle anhand des models erstellen
        $tool->createSchema($classes);
    }


    public function uninstall()
    {

        // löschen der tabelle bei uninstall
        $em = Shopware()->Models();
        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

        $classes = array(
            $em->getClassMetadata('Shopware\CustomModels\PmDoctrineModel\MeinModel')
        );
        $tool->dropSchema($classes);

        return true;
    }

    /**
     * Returns plugin info
     *
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => '1.0.0',
            'label' => 'Beispiel Plugin 5',
            'author' => 'Philipp Mahlow',
            'copyright' => '2016 - Philipp Mahlow',
            'description' => 'Ein langer Text...'
        );
    }

}