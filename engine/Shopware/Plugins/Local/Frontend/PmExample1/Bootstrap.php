<?php

class Shopware_Plugins_Frontend_PmExample1_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{


    /**
     * Plugin installer
     *
     * @return bool
     */
    public function install(){
        //neu installation
        return TRUE;
    }

    /**
     * Plugin uninstaller
     *
     * @return bool
     */
    public function uninstall(){
        return TRUE;
    }

    /**
     * plugin activation
     *
     * @return bool
     */
    public function enable(){
        return TRUE;
    }


    /**
     * plugin deactivation
     *
     * @return bool
     */
    public function disable() {
        return TRUE;
    }

    /**
     * @param string $version
     * @return bool
     */
    public function update($version)
    {
        switch($version){
            case '1.0.0':
                //update routine von 1.0.0 auf 1.2.0
                break;
            case '1.1.0':
                //update routine von 1.1.0 auf 1.2.0
                break;
        }

        return TRUE;
    }


    /**
     * Returns plugin info
     *
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => '1.2.0',
            'label' => 'Beispiel Plugin 1',
            'author' => 'Philipp Mahlow',
            'copyright' => '2016 - Philipp Mahlow',
            'description' => 'Ein langer Text...'
        );
    }

}