<?php

class Shopware_Plugins_Frontend_PmExample2_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{


    /**
     * Plugin installer
     *
     * @return bool
     */
    public function install(){
        //subscribe PreDispatchEvent
        $this->subscribeEvent(
            'Enlight_Controller_Action_PreDispatch_Frontend_Listing',
            'onPreDispatch'
        );

        // controller action Listing index komplett überschreiben
        $this->subscribeEvent(
            'Enlight_Controller_Action_Frontend_Listing_Index',
            'onActionFrontendListingIndex'
        );

        // frontend listing postDispatch
        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Frontend_Listing',
            'onPostDispatch'
        );

        return TRUE;
    }


    /**
     *
     * @param Enlight_Event_EventArgs $args
     */
    public function onActionFrontendListingIndex(Enlight_Event_EventArgs $args)
    {
        /**
         * @var Enlight_Controller_Action $subject
         */
        $subject = $args->getSubject();

        //echo " action ";

        //hier true zurückgeben, wenn die action überschrieben werden soll
        //return TRUE;
    }



    /**
     * event listener for general pre dispatch event
     *
     * @param Enlight_Event_EventArgs $args
     */
    public function onPreDispatch(Enlight_Event_EventArgs $args)
    {
        /**
         * @var Enlight_Controller_Action $subject
         */
        $subject = $args->getSubject();

        //echo "before ";
    }

    /**
     * event listener for general pre dispatch event
     *
     * @param Enlight_Event_EventArgs $args
     */
    public function onPostDispatch(Enlight_Event_EventArgs $args)
    {
        /**
         * @var Enlight_Controller_Action $subject
         */
        $subject = $args->getSubject();

        //die('after');

        //eigene view variable assignen
        $subject->View()->assign('testVariable', 'Test123');

        //view verzeichnis hinzufügen
        $subject->View()->addTemplateDir($this->Path() . 'Views/');


        // bestehende view-variablen modifizieren
        $sArticles = $subject->View()->getAssign('sArticles');

        foreach($sArticles as &$article){
            $article['articleName'] .= ' TEST';
        }

        $subject->View()->assign('sArticles', $sArticles);
    }


    /**
     * Returns plugin info
     *
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => '1.0.0',
            'label' => 'Beispiel Plugin 2',
            'author' => 'Philipp Mahlow',
            'copyright' => '2016 - Philipp Mahlow',
            'description' => 'Ein langer Text...'
        );
    }

}