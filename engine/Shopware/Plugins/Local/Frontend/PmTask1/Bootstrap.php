<?php

class Shopware_Plugins_Frontend_PmTask1_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{


    /**
     * Plugin installer
     *
     * @return bool
     */
    public function install(){
        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Frontend_Detail',
            'onPostDispatch'
        );

        $this->registerController('frontend', 'PmTask1');

        return TRUE;
    }


    /**
     * @param Enlight_Event_EventArgs $args
     */
    public function onPostDispatch(Enlight_Event_EventArgs $args)
    {
        /**
         * @var Enlight_Controller_Action $subject
         */
        $subject = $args->getSubject();

        if($subject->Request()->getActionName() == 'index')
        {
            //view verzeichnis hinzufügen
            $subject->View()->addTemplateDir($this->Path() . 'Views/');
        }

    }


    /**
     * Returns plugin info
     *
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => '1.0.0',
            'label' => 'Beispiel Aufgabe',
            'author' => 'Philipp Mahlow',
            'copyright' => '2016 - Philipp Mahlow',
            'description' => 'Ein langer Text...'
        );
    }

}