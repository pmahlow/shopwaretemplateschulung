<?php

class Shopware_Controllers_Frontend_PmTask1
    extends Enlight_Controller_Action
{
    /**
     * @var \Shopware\Models\Article\Article
     */
    protected $article;


    public function preDispatch()
    {
        $articleId = $this->Request()->getParam('articleId');

        if($articleId) {

            $repository = Shopware()
                ->Models()
                ->getRepository('Shopware\Models\Article\Article');

            $this->article = $repository->find($articleId);

            if(!$this->article){
                $this->redirect(array(
                    'controller' => 'index'
                ));
            }
        }
    }


    public function indexAction(){

        $this->View()->assign('article', $this->article);
    }
}